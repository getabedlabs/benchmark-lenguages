Basado en

https://www.toptal.com/back-end/server-side-io-performance-node-php-java-go
https://peabody.io/post/server-env-benchmarks/

## Go

AB: ab -n 2000 -c 300 'http://127.0.0.1:4000/test?n=1'

Darwin BenderPro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jun 15 17:36:27 PDT 2017; root:xnu-3789.70.16~2/RELEASE_X86_64 x86_64
Comando: go run server.go

    Concurrency Level:      300
    Time taken for tests:   1.330 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      0 bytes
    HTML transferred:       0 bytes
    Requests per second:    1503.25 [#/sec] (mean)
    Time per request:       199.568 [ms] (mean)
    Time per request:       0.665 [ms] (mean, across all concurrent requests)
    Transfer rate:          0.00 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   41  26.6     42     111
    Processing:     1  149  90.1    145     591
    Waiting:        0    0   0.0      0       0
    Total:          1  190  79.4    180     656


go version go1.2.1 linux/amd64
go run test_2.go

    Concurrency Level:      300
    Time taken for tests:   3.169 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      362000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    631.10 [#/sec] (mean)
    Time per request:       475.362 [ms] (mean)
    Time per request:       1.585 [ms] (mean, across all concurrent requests)
    Transfer rate:          111.55 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   73 258.8      0    1001
    Processing:     7  367 121.5    404     771
    Waiting:        7  360 119.8    400     771
    Total:         11  440 290.8    413    1699

## Node

AB: ab -n 2000 -c 300 'http://127.0.0.1:3000/test?n=1'

Darwin BenderPro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jun 15 17:36:27 PDT 2017; root:xnu-3789.70.16~2/RELEASE_X86_64 x86_64
Comando: node test_2.js

    Concurrency Level:      300
    Time taken for tests:   1.415 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    1413.59 [#/sec] (mean)
    Time per request:       212.225 [ms] (mean)
    Time per request:       0.707 [ms] (mean, across all concurrent requests)
    Transfer rate:          191.88 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   14  32.9      0     130
    Processing:    50  186  38.2    191     256
    Waiting:       50  186  38.2    191     256
    Total:         74  200  47.5    198     348

Linux benderinux 3.13.0-83-generic #127-Ubuntu SMP Fri Mar 11 00:25:37 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
Comando: node test_2.js
    Concurrency Level:      300
    Time taken for tests:   2.273 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    879.87 [#/sec] (mean)
    Time per request:       340.958 [ms] (mean)
    Time per request:       1.137 [ms] (mean, across all concurrent requests)
    Transfer rate:          119.44 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0  100 297.1      0    1001
    Processing:    96  206  82.8    187     562
    Waiting:       96  206  82.8    187     562
    Total:        113  305 311.9    192    1325

Darwin BenderPro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jun 15 17:36:27 PDT 2017; root:xnu-3789.70.16~2/RELEASE_X86_64 x86_64
Comando: pm2 start test_2.js -i 0

    Concurrency Level:      300
    Time taken for tests:   1.122 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    1782.20 [#/sec] (mean)
    Time per request:       168.331 [ms] (mean)
    Time per request:       0.561 [ms] (mean, across all concurrent requests)
    Transfer rate:          241.92 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   14  15.4     10     101
    Processing:     6  142  49.4    155     320
    Waiting:        6  136  47.6    147     304
    Total:         31  156  41.0    165     329


Linux benderinux 3.13.0-83-generic #127-Ubuntu SMP Fri Mar 11 00:25:37 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
Comando: pm2 start test_2.js -i 0

    Concurrency Level:      300
    Time taken for tests:   4.371 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    457.57 [#/sec] (mean)
    Time per request:       655.635 [ms] (mean)
    Time per request:       2.185 [ms] (mean, across all concurrent requests)
    Transfer rate:          62.11 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   24 148.2      0    1002
    Processing:    29  592 135.9    622     752
    Waiting:       28  592 135.9    621     752
    Total:         44  616 210.2    622    1735


## PHP 5.5 (Nginx + FPM-Port)

Linux benderinux 3.13.0-83-generic #127-Ubuntu SMP Fri Mar 11 00:25:37 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
ab: ab -n 2000 -c 300 'http://labs.dev:8080/langs/test_2.php'

    Concurrency Level:      300
    Time taken for tests:   17.001 seconds
    Complete requests:      2000
    Failed requests:        1215
    (Connect: 0, Receive: 0, Length: 1215, Exceptions: 0)
    Non-2xx responses:      785
    Total transferred:      548350 bytes
    HTML transferred:       219845 bytes
    Requests per second:    117.64 [#/sec] (mean)
    Time per request:       2550.224 [ms] (mean)
    Time per request:       8.501 [ms] (mean, across all concurrent requests)
    Transfer rate:          31.50 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0  176 610.6      0    7015
    Processing:     8 2187 1206.5   2374    6244
    Waiting:        7 2187 1206.6   2373    6242
    Total:          8 2363 1373.2   2398    9420

## PHP 7.1 (Nginx + FPM-Sock)

Linux xenial16 4.4.0-83-generic #106-Ubuntu SMP Mon Jun 26 17:54:43 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
ab: ab -n 2000 -c 300 'http://labs.dev:1680/langs/test_2.php'

    Concurrency Level:      300
    Time taken for tests:   1.236 seconds
    Complete requests:      2000
    Failed requests:        474
    (Connect: 0, Receive: 0, Length: 474, Exceptions: 0)
    Non-2xx responses:      1526
    Total transferred:      622958 bytes
    HTML transferred:       308068 bytes
    Requests per second:    1617.57 [#/sec] (mean)
    Time per request:       185.463 [ms] (mean)
    Time per request:       0.618 [ms] (mean, across all concurrent requests)
    Transfer rate:          492.03 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0    1   1.9      0       7
    Processing:    30  144 139.5     77     494
    Waiting:       30  144 139.5     77     494
    Total:         35  145 139.6     77     494