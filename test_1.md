Test 1

Darwin BenderPro.local 16.7.0 Darwin Kernel Version 16.7.0: Thu Jun 15 17:36:27 PDT 2017; root:xnu-3789.70.16~2/RELEASE_X86_64 x86_64


## C++

```sh
time ./test_1.bin
sum: 887459712
./test_1.bin  0.25s user 0.01s system 94% cpu 0.273 total
```



## Go 1.8.3

```sh
time go run test_1.go
4999999950000000
go run test_1.go  0.22s user 0.44s system 74% cpu 0.899 total
```


## PHP 7.1.3

```sh
time php -f test_1.php
4999999950000000
php -f test_1.php  2.37s user 0.05s system 88% cpu 2.721 total
```


## Node JS 8.2.1

```sh
time node test_1.js
4999999950000000
node test_1.js  0.51s user 0.08s system 74% cpu 0.789 total
```


## Ruby 2.3.4

```sh
time ruby test_1.rb5000000050000000
ruby test_1.rb  19.52s user 0.42s system 84% cpu 23.714 total
```


## Python 2.7.13

```sh
time python test_1.py
100000000
python test_1.py  26.20s user 7.42s system 88% cpu 38.043 total
```


## Python 3

```sh
time python3 test_1.py
100000000
python3 test_1.py  13.69s user 0.14s system 97% cpu 14.196 total
```