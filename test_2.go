package main

import (
	"io/ioutil"
	"net/http"
	"fmt"
	"crypto/sha256"
	"log"
)

func main() {

	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadFile("test_2.dat")
		if err != nil { panic(err) }
		s := ""
		nstr := r.FormValue("n")
		var n int
		fmt.Sscanf(nstr, "%d", &n)
		for i := 0; i < n; i++ {
			h := sha256.New()
			h.Write(b)
			s = fmt.Sprintf("%x", h.Sum(nil))
		}
		w.Write([]byte(s))
	})

	log.Fatal(http.ListenAndServe("127.0.0.1:4000", nil))
}