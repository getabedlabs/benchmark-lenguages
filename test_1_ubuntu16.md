Linux xenial16 4.4.0-83-generic #106-Ubuntu SMP Mon Jun 26 17:54:43 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

## C++

```sh
time ./test_1.bin
sum: 887459712

real	0m0.257s
user	0m0.252s
sys	0m0.000s
```

## Go 1.8

```sh
time go run test_1.go
4999999950000000

real	0m0.213s
user	0m0.148s
sys	0m0.040s
```

## PHP 7.1

```sh
time php -f test_1.php
4999999950000000

real	0m1.838s
user	0m1.816s
sys	0m0.020s
```

## PHP over HHVM 3.21

```sh
time hhvm -f test_1.php
4999999950000000

real	0m2.169s
user	0m2.136s
sys	0m0.024s
```

## Node 6.11

time node test_1.js
4999999950000000

real	0m0.220s
user	0m0.212s
sys	0m0.004s


## Ruby 2.3

```sh
time ruby test_1.rb
5000000050000000

real	0m13.013s
user	0m12.960s
sys	0m0.020s
```


## Python 27

```sh
time python test_1.py
Killed

real	0m8.135s
user	0m0.220s
sys	0m3.428s
```


## Python 3

```sh
time python3 test_1.py
100000000

real	0m9.670s
user	0m9.644s
sys	0m0.008s
```