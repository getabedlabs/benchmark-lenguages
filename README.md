# Benchmark de lenguages

## Test 1

Una suma del 0 al 100,000,000

Basado en: https://hashnode.com/post/comparison-nodejs-php-c-go-python-and-ruby-cio352ydg000ym253frmfnt70

El pseudo código es

    sum = 0
    for (i = 1; i < 100000000; i++) {
        sum = sum + i
    }
    imprimir sum

## Test 2

Revisar como se comportan los lenguages con bloqueo de archivos a nivel de sistema operativo,
con varios bloques por proceso con varios procesos a la vez.

Basado en:
 + https://www.toptal.com/back-end/server-side-io-performance-node-php-java-go
 + https://peabody.io/post/server-env-benchmarks/

El pseudo código es

    contenido_archivo = leer_contenido("archivo_dummy")
    veces_a_generar_hash_por_proceso = $_REQUEST['n']

    for (i = 0; i < veces_a_generar_hash_por_proceso; i++) {
        hash = hash('sha256', contenido_archivo)
    }

    imprimir hash


## Resultado

Se corrieron pruebas en Mac instalando los entornos con Brew y en Linux Ubuntu 16 en una maquina virtual a través de Vagrant.

Los resultados se pueden consultar en los archivos .md

El resumen es, sin bloqueos hay un empate técnico entre Go Lang, C++ y Node, con bloques es distinto, corriendo la prueba en Mac hay un empate técnico entre Go, Node, PHP, pero al correr sobre Ubuntu el mejor es PHP.