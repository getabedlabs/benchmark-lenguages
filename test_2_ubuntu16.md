Linux xenial16 4.4.0-83-generic #106-Ubuntu SMP Mon Jun 26 17:54:43 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

# Go Lang 1.8

go run test_2.go
ab -n 2000 -c 300 'http://127.0.0.1:4000/test?n=1'

    Concurrency Level:      300
    Time taken for tests:   2.275 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      362000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    878.97 [#/sec] (mean)
    Time per request:       341.310 [ms] (mean)
    Time per request:       1.138 [ms] (mean, across all concurrent requests)
    Transfer rate:          155.36 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   41 185.0      0    1015
    Processing:     5  234 107.2    212     844
    Waiting:        5  217 107.5    193     839
    Total:          5  275 204.0    221    1557


## PHP over HHVM 3.21

ab -n 2000 -c 300 'http://127.0.0.1:1680/test_2.php?n=1'

    Concurrency Level:      300
    Time taken for tests:   3.835 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      468000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    521.55 [#/sec] (mean)
    Time per request:       575.209 [ms] (mean)
    Time per request:       1.917 [ms] (mean, across all concurrent requests)
    Transfer rate:          119.18 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0    2   4.0      0      16
    Processing:    16  532 118.8    572     597
    Waiting:       16  532 118.8    572     597
    Total:         30  534 115.0    572     597


## PHP 7.1 (Nginx + FPM-Sock)

ab -n 2000 -c 300 'http://labs.dev:1680/langs/test_2.php'

    Concurrency Level:      300
    Time taken for tests:   1.236 seconds
    Complete requests:      2000
    Failed requests:        474
    (Connect: 0, Receive: 0, Length: 474, Exceptions: 0)
    Non-2xx responses:      1526
    Total transferred:      622958 bytes
    HTML transferred:       308068 bytes
    Requests per second:    1617.57 [#/sec] (mean)
    Time per request:       185.463 [ms] (mean)
    Time per request:       0.618 [ms] (mean, across all concurrent requests)
    Transfer rate:          492.03 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0    1   1.9      0       7
    Processing:    30  144 139.5     77     494
    Waiting:       30  144 139.5     77     494
    Total:         35  145 139.6     77     494

# Node 6.11

node test_2.js
ab -n 2000 -c 300 'http://127.0.0.1:3000/test?n=1'

    Concurrency Level:      300
    Time taken for tests:   1.898 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    1053.73 [#/sec] (mean)
    Time per request:       284.704 [ms] (mean)
    Time per request:       0.949 [ms] (mean, across all concurrent requests)
    Transfer rate:          143.04 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0   85 277.9      0    1001
    Processing:    86  180  84.6    152     892
    Waiting:       86  180  84.6    151     892
    Total:         86  265 318.5    153    1892


pm2 start test_2.js -i 0
ab -n 2000 -c 300 'http://127.0.0.1:3000/test?n=1'

    Concurrency Level:      300
    Time taken for tests:   3.433 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      278000 bytes
    HTML transferred:       128000 bytes
    Requests per second:    582.51 [#/sec] (mean)
    Time per request:       515.011 [ms] (mean)
    Time per request:       1.717 [ms] (mean, across all concurrent requests)
    Transfer rate:          79.07 [Kbytes/sec] received

    Connection Times (ms)
                min  mean[+/-sd] median   max
    Connect:        0    1   3.4      0      23
    Processing:    27  479 105.5    494     591
    Waiting:       26  479 105.5    494     591
    Total:         45  480 102.9    494     598