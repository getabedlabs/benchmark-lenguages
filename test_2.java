<%@ page import="java.nio.file.Files,java.nio.file.FileSystems,java.security.MessageDigest" %>
<%!
public static String bytesToHex(byte[] in) {
    final StringBuilder builder = new StringBuilder();
    for(byte b : in) {
            builder.append(String.format("%02x", b));
    }
    return builder.toString();
}
%>
<%

String nstr = request.getParameter("n");
int n = Integer.parseInt(nstr);

String s = "";
byte[] b = Files.readAllBytes(FileSystems.getDefault().getPath("/tmp/data"));

for (int i = 0; i < n; i++) {
	MessageDigest md = MessageDigest.getInstance("SHA-256");

	md.update(b);
	byte[] d = md.digest();
	s = bytesToHex(d);
}

out.print(s);

%>