const http = require('http')
const fs = require('fs')
const sha256 = require('sha256')
const url = require('url')
const port = 3000

const requestHandler = (request, response) => {
	fs.readFile('test_2.dat', function(err, data) {
		if (err) { response.end("error: " + err) }
		var u = url.parse(request.url, true);
		var n = parseInt(u.query.n);
		var resstr = '';
		for (var i = 0; i < n; i++) {
			resstr = sha256(data);
		}
		response.end(resstr);
	})
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
	if (err) {
		return console.log('uh oh', err)
	}
	console.log('listening on '+port)
})